const n = prompt('Please enter the number');
const result = [];

for (let i = 1; i <= n; i++) {
  if (i % 5 !== 0) continue;

  result.push(i);
}

if (result.length) {
  for (let i = 0; i < result.length; i++) {
    console.log(result[i]);
  }
} else {
  console.log('Sorry, no numbers');
}